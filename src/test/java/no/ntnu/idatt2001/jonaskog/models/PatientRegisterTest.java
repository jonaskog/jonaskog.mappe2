package no.ntnu.idatt2001.jonaskog.models;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for adminstrating patientregister
 */
class PatientRegisterTest {
    PatientRegister register1 = new PatientRegister();
    @Test
    void addPatient() {
        register1.addPatient(new Patient("test","test","test"));
        //3 expevted since there are 3 pasients in register
        assertEquals(3,register1.getPasienter().size());
    }

    @Test
    void removePatient() {
        register1.addPatient(new Patient("test","test","test"));
        register1.removePatient("test");
        assertEquals(2,register1.getPasienter().size());
    }


    @Test
    void addPatientsFormListe() {
        ArrayList<Patient> newList = new ArrayList<>();
        newList.add(new Patient("test","test","test"));
        newList.add(new Patient("test","test","test"));
       register1.addPatientsFromList(newList);

       assertEquals(4,register1.getPasienter().size());
    }


}