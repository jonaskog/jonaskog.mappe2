module no.ntnu.idatt2001.jonaskog {
    requires javafx.controls;
    requires javafx.fxml;

    opens no.ntnu.idatt2001.jonaskog to javafx.fxml;
    exports no.ntnu.idatt2001.jonaskog;
    exports no.ntnu.idatt2001.jonaskog.models;
    opens no.ntnu.idatt2001.jonaskog.models to javafx.fxml;
    exports no.ntnu.idatt2001.jonaskog.controller;
    opens no.ntnu.idatt2001.jonaskog.controller to javafx.fxml;
    exports no.ntnu.idatt2001.jonaskog.popup;
    opens no.ntnu.idatt2001.jonaskog.popup to javafx.fxml;


}
