package no.ntnu.idatt2001.jonaskog.controller;
import javafx.scene.control.*;
import no.ntnu.idatt2001.jonaskog.models.patientFilereader;

import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import no.ntnu.idatt2001.jonaskog.popup.PatientWindow;
import no.ntnu.idatt2001.jonaskog.models.Patient;
import no.ntnu.idatt2001.jonaskog.models.PatientRegister;


/**
 * Primary controller class
 */
public class PrimaryController implements Initializable {
    private Patient selectedPatient;//reference to a patient
    private PatientRegister patientRegister;//reference to patient register
    private ObservableList<Patient> patientObservableList; // list attached to tabelview
    @FXML
    private Button addPatientButton,editPatientButton,deletePatientButton;
    @FXML
    private TableView<Patient> patientTabelView;
    @FXML
    private TableColumn<Patient,String> firstNameColumn,lastNameColumn,socialSecColumn;//each column



    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.patientRegister = new PatientRegister();
        //create columns
        this.firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        this.lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastname"));
        this.socialSecColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecNumber"));

        this.patientObservableList =
                FXCollections.observableArrayList(this.patientRegister.getPasienter());




        //selects patient from tabelview
        patientTabelView.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getButton().equals(MouseButton.PRIMARY) || mouseEvent.getButton().equals(MouseButton.SECONDARY)) {
                selectedPatient = patientTabelView.getSelectionModel().getSelectedItem();
                System.out.println("Selected item: " + selectedPatient);
            }
        });
        //updates tabelview
        this.patientTabelView.setItems(this.patientObservableList);
    }

    /**
     * Updates tabelview if Patients are added.
     */
    public  void updateObservableList() { this.patientObservableList.setAll(this.patientRegister.getPasienter()); }

    /**
     * gets popupwindow , and adds patient to register
     */
    @FXML
    public void addPatientWindow(){
        PatientWindow addWindow = new PatientWindow();
        Optional<Patient> result = addWindow.showAndWait();
        if(result.isPresent()){
            try {
                Patient newPatient = result.get();//result form addWindow can return a patient
                //adds patient to registers
                patientRegister.addPatient(newPatient);
                System.out.println(newPatient);

            }catch (IllegalArgumentException e){
                System.out.println("patient not added");
            }
            //when done uppdates tabelView
            updateObservableList();
        }
    }

    /**
     * gets editPatient popupwindow and edits patient
     */
    @FXML
    public void editPatientWindow(){

        if (selectedPatient != null) {
            PatientWindow dialog = new PatientWindow(selectedPatient);

            Optional<Patient> result = dialog.showAndWait();

            if (result.isPresent()) {
                selectedPatient = result.get();
                updateObservableList();
            }
        }
    }

    public void deletePatientWindow(){

        if(selectedPatient != null) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setHeaderText("Delete Confirmation");
            alert.setContentText("Are you sure you want to delete patient \n\"" + selectedPatient);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.OK) {
                patientRegister.removePatient(selectedPatient.getSocialSecNumber());
                updateObservableList();
                System.out.println("Removed patient \"" + selectedPatient + "\" from the register");
                selectedPatient = null;
            } else {
                System.out.println("Patient deletion cancelled.");
                }
            }
        }

    /**
     * Imports from csv handed out in project assignment.
     * @throws Exception
     */
    public void importCSVButton() throws Exception {
        patientRegister.deletePatients();
        ArrayList<Patient> listFromCsv= new ArrayList<>();
        patientFilereader filereader = new patientFilereader("C:\\Users\\Jolse\\no\\mappeprove.peronreg\\src\\main\\resources\\no\\ntnu\\idatt2001\\jonaskog\\Patients.csv");
        patientRegister.addPatientsFromList(filereader.getDataFromCsv());
        updateObservableList();
        }

    public void aboutButton(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("ABOUT:");
        alert.setContentText("This application was created by JOnas olsen\n" +
                "Here you can import a csv file from directory\n" +
                "One can also add, edit and delete patients\n" +
                "Here is link to source code https://gitlab.stud.idi.ntnu.no/jonaskog");
        alert.showAndWait();
        }
    }




