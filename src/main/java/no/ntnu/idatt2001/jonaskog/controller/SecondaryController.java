package no.ntnu.idatt2001.jonaskog.controller;

import java.io.IOException;
import javafx.fxml.FXML;
import no.ntnu.idatt2001.jonaskog.App;

public class SecondaryController {

    @FXML
    private void switchToPrimary() throws IOException {
        App.setRoot("primary");
    }
}