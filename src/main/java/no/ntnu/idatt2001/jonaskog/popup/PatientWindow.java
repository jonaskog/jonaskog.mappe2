package no.ntnu.idatt2001.jonaskog.popup;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import no.ntnu.idatt2001.jonaskog.controller.PrimaryController;
import no.ntnu.idatt2001.jonaskog.models.Factory;
import no.ntnu.idatt2001.jonaskog.models.Patient;
import no.ntnu.idatt2001.jonaskog.models.PatientRegister;

/**
 * Class for adding and editing patient
 * @author Jolse
 */
public class PatientWindow extends Dialog<Patient> {

    //refrences to mode , patient
    private  Modus mode;
    private Patient aPatient = null;
    private PatientRegister patientRegister;

    public enum Modus{
        ADD, EDIT
    }
    /**
     * Constructor for add patient popup window
     */
    public PatientWindow(){
        super();
        this.mode = Modus.ADD;
        creatPopup();
    }

    /**
     * Constructor for edit patient popup
     * @param patient
     */
    public PatientWindow(Patient patient) {
        super();
        mode = Modus.EDIT;
        this.aPatient = patient;
        creatPopup();
    }

    public void creatPopup(){
        getDialogPane().getButtonTypes().addAll(ButtonType.OK,ButtonType.CANCEL);

        switch (mode){
            case ADD:
                setTitle("Add patient");
                break;
            case EDIT:
                setTitle("EDIT patient");
                break;
        }
        setupPopup();
    }

    public void setupPopup(){
        Factory factory = new Factory();

        Label firstNameLabel = (Label) factory.create("label");
        firstNameLabel.setText("First name:");
        TextField firstNameInput = (TextField) factory.create("textfield");


        Label lastNameLabel = (Label) factory.create("label");
        lastNameLabel.setText("Last name:");
        TextField lastNameInput = (TextField) factory.create("textfield");


        Label socialSecurityNumberLabel = (Label) factory.create("label");
        socialSecurityNumberLabel.setText("Social security number:");
        TextField socialSecurityNumberInput = (TextField) factory.create("textfield");

        HBox firstNameBox = (HBox) factory.create("hbox");
        firstNameBox.getChildren().setAll(firstNameLabel,firstNameInput);
        firstNameBox.setSpacing(86);

        HBox lastNameBox = (HBox) factory.create("hbox");
        lastNameBox.getChildren().setAll(lastNameLabel,lastNameInput);
        lastNameBox.setSpacing(86);


        HBox socialSecurityNumberBox = (HBox) factory.create("hbox");
        socialSecurityNumberBox.getChildren().setAll(socialSecurityNumberLabel,socialSecurityNumberInput);
        socialSecurityNumberBox.setSpacing(20);


        VBox root = (VBox) factory.create("vbox");
        root.setSpacing(10);

        if(mode == Modus.ADD){
            root.getChildren().setAll(firstNameBox,lastNameBox,socialSecurityNumberBox);
            firstNameInput.setPromptText("First name");
            lastNameInput.setPromptText("Last name");

            setResultConverter((ButtonType button) -> { // what to return
                Patient patient = null;
                String firstName = firstNameInput.getText();
                String lastName = lastNameInput.getText();
                String socialSecurityNumber = socialSecurityNumberInput.getText();

                if (button == ButtonType.OK) {
                    try {
                        patient = new Patient(firstName,lastName,socialSecurityNumber);

                        // check for right length in personal number
                        if(patient.getSocialSecNumber().length() != 11){
                            mistakePopup.showInfo("Person number must have 11 numbers\n Person not added");
                            patient = null;
                        }
                    } catch (IllegalArgumentException e) {
                      System.out.println("Invalid input Patient was not added.\n" + e.getMessage());
                    }
                }
                return patient;
            });
            //for when user want to edit
        } else {root.getChildren().setAll(firstNameBox,lastNameBox);
            firstNameInput.setText(aPatient.getName());
            lastNameInput.setText(aPatient.getLastname());

            setResultConverter((ButtonType button) -> { // what to return
                Patient patient = aPatient;
                String firstName = firstNameInput.getText();
                String lastName = lastNameInput.getText();
                if (button == ButtonType.OK) {
                    try {
                        patient.setName(firstName);
                        patient.setLastname(lastName);
                    } catch (IllegalArgumentException e) {
                       System.out.println("Invalid inputPatient was not edited:\n" + e.getMessage());
                    }
                }
                return patient;
            });
        }
        getDialogPane().setContent(root);
    }
}
