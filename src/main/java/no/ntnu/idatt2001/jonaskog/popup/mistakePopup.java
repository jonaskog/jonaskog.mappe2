package no.ntnu.idatt2001.jonaskog.popup;

import javafx.scene.control.Alert;

import java.util.Locale;

/**
 * Class for alrets
 */
public class mistakePopup {
    /**
     * Message for when user does a trivial mistake
     * @param alertMessage
     */
    public static void showInfo( String alertMessage){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setContentText(alertMessage.toUpperCase(Locale.ROOT));

        alert.showAndWait();
    }

    /**
     * Message for when user does a serious mistake
     * @param alertMessage
     */
    public static void showError(String alertMessage){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setContentText(alertMessage);
        alert.showAndWait();
    }

    /**
     * Confirmation message
     * @param alertMessage
     */
    public static void showConfirmation(String alertMessage){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setContentText(alertMessage);
    }

}
