package no.ntnu.idatt2001.jonaskog.models;

import no.ntnu.idatt2001.jonaskog.models.Patient;
/**
 * class for reading of files
 */
import java.io.*;
import java.util.ArrayList;

public class patientFilereader {
    private final File file;


    /**
     * Constructs a file
     * @param path
     */
    public patientFilereader(String path) throws Exception {
        if (!path.endsWith(".csv")) {
            throw new Exception(path + "is not a file");
        }
        this.file = new File(path);
    }

    /**
     * Gets info from csv, and puts it in a arryList
     *
     * @return
     * @throws IOException
     */
    public  ArrayList<Patient> getDataFromCsv() throws IOException {
        ArrayList<Patient> csvData = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            String line;
            reader.readLine();
            while ((line = reader.readLine()) != null) {
                String[] values = line.split(";");
                //skips third value since that is practitioner, in which we dont use
                Patient patient = new Patient(values[0], values[1], values[3]);
                csvData.add(patient);

            }
            reader.close();
        } catch (IOException e) {

            System.out.println("feil skjedde med lesingen av file");
        }

        return csvData;
    }

    /**
     * Writes to file
     * @param patients
     */
    public void writeTofile(ArrayList<Patient> patients){
        try{
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write("First name; Last name; Social security number\n");
            for (Patient patient : patients){
                String data = patient.getName() + " ;"+ patient.getLastname()+ " ;"+ patient.getSocialSecNumber()+ " \n";
                writer.write(data);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}