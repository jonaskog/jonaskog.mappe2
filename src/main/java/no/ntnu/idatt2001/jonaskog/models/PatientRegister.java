package no.ntnu.idatt2001.jonaskog.models;

import no.ntnu.idatt2001.jonaskog.models.Patient;

import java.util.ArrayList;
import java.util.List;

/**
 * CLass for making a patient register
 */

public class PatientRegister {
    private  ArrayList<Patient> pasienter;

    /**
     * Constructor
     */
    public PatientRegister() {
        this.pasienter = new ArrayList<>();
        fillWithPatients();
    }
    public void fillWithPatients(){
        pasienter.add(new Patient("Random","Random","1232122222"));
        pasienter.add(new Patient("Sjef","Sjef","1232122222"));
    }

    public  void addPatient(String name, String lastname, String socialSecNumber){
        Patient nyPasient = new Patient(name,lastname,socialSecNumber);
        this.pasienter.add(nyPasient);
    }


    public void removePatient(String socialSecNumber){
        pasienter.removeIf(p -> p.getSocialSecNumber().equals(socialSecNumber));
    }

    public List<Patient> getPasienter() {
        return this.pasienter;
    }


    public void addPatient(Patient newPatient) {
        this.pasienter.add(newPatient);
    }

    /**
     * adds a arrylist with pasients to patients register
     * @param nyListe
     */
    public void addPatientsFromList(ArrayList<Patient> nyListe){
        for (Patient p :nyListe){
            addPatient(p);
        }
    }
    public  void deletePatients(){
        pasienter.clear();
    }
}
