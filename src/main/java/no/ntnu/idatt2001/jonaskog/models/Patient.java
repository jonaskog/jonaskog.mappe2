package no.ntnu.idatt2001.jonaskog.models;

import java.util.Objects;

/**
 * Model for patient object
 * @author Jolse
 */
public class Patient {
    private String name;
    private String lastname;
    private String socialSecNumber;


    public Patient(String name, String lastname, String socialSecNumber) {
        this.name = name;
        this.lastname = lastname;
        this.socialSecNumber = socialSecNumber;

    }


    //Set og get metoder
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getSocialSecNumber() {
        return socialSecNumber;
    }

    public void setSocialSecNumber(String socialSecNumber) {
        this.socialSecNumber = socialSecNumber;
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Patient)) return false;
        Patient patient = (Patient) o;
        return Objects.equals(getSocialSecNumber(), patient.getSocialSecNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSocialSecNumber());
    }

    @Override
    public String toString() {
        return "Patient{" +
                "name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", socialSecNumber=" + socialSecNumber +
                +
                '}';
    }
}
